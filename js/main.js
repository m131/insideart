'use strict';

document.addEventListener('DOMContentLoaded', () => {
  /*
   * Menu toggle button
   */
  const menu = document.querySelector('.menu');
  const menuButton = document.querySelector('.navbar__toggle');
  menuButton.addEventListener('click', e => {
    e.stopPropagation();
    menu.classList.toggle('menu--active');
    menuButton.classList.toggle('navbar__toggle--active');
  })
  
  document.addEventListener('click', e => {
    const checkMenuOpen = !e.target.matches('.menu') && menu.classList.contains('menu--active');
    if (checkMenuOpen){
      menu.classList.toggle('menu--active');
      menuButton.classList.toggle('navbar__toggle--active');
    }

  })

  /*
   * Section info interaction
   */
  const sectionNav = document.querySelectorAll('.section .section__navbar');
  sectionNav.forEach(nav => {
    const sectionContainer = nav.parentElement;
    const sectionTitle = sectionContainer.querySelector('.section__title');
    const sectionContent = sectionContainer.querySelector('.section__inner-container');
    const info = sectionContainer.querySelector('.info');
    const closeInfo = info.querySelector('.info__close');
    const navLinks = nav.querySelectorAll('.section__menu-link');

    navLinks.forEach(link => {
      link.addEventListener('click', e => {
        e.preventDefault();
        const currentLink = e.target.classList.contains('section__menu-link')? e.target: e.target.parentElement;
        const linkIndex = ([...navLinks].findIndex(item => item === currentLink));
        console.log(linkIndex);
        sectionTitle.classList.toggle('inactive');
        sectionContent.classList.toggle('inactive');
        nav.classList.toggle('inactive');
        info.classList.toggle('active');
        const infoContainer = info.querySelector('.info__container');
        const infoElement = info.querySelectorAll('.info__article')[linkIndex];
        const gap = window.getComputedStyle(infoContainer).getPropertyValue('grid-column-gap').slice(0, -2);
        infoContainer.scrollTo(infoElement.offsetLeft - gap, 0);
      });
    })
    
    closeInfo.addEventListener('click', () => {
      info.classList.toggle('active');
      sectionTitle.classList.toggle('inactive');
      sectionContent.classList.toggle('inactive');
      nav.classList.toggle('inactive');
    })
  })

})
